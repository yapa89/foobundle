<?php

namespace Yapa89\FooBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FooCommand extends Command
{
    public function configure()
    {
        $this->setName('foo:hello');
    }

    protected function execute(InputInterface $input, OutputInterface $output) : int
    {
        $output->writeln('Hello from Foo!');
        return Command::SUCCESS;
    }

}
